<?php


defined('BASEPATH') or exit('No direct script access allowed');

class Contact extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->check_login();
        if ($this->session->userdata('id_role') != "1") {
            redirect('', 'refresh');
        }
        $this->load->helper('login_helper');

    }

    public function index()
    {
        $site = $this->Konfigurasi_model->listing();
        $data = array(
            'title'                 => 'Dashboard | '.$site['nama_website'],
            'favicon'               => $site['favicon'],
            'site'                  => $site,
        );
        $this->template->load('layout/template', 'admin/contact', $data);
    }

    function actionEdit($password = null)
        {
            $password = $this->input->post('password');
            $id = $this->input->post('id');
           

            //jalankan validasi
                if(!empty($id))
                {
                    $data = array(
                    'id_role'        => $this->input->post('id_role'),
                    'nidn'           => $this->input->post('nidn'),
                    'username'       => $this->input->post('username'),
                    'password'       => get_hash($password),
                    'first_name'     => $this->input->post('first_name'),
                    'last_name'      => $this->input->post('last_name'),
                    'email'          => $this->input->post('email'),
                    'phone'          => $this->input->post('phone'),
                    'photo'          => $this->input->post('photo')
                    );
                    $this->db->set($data);
                    $this->Pengawas_model->updatePengawas($id, $data);


                    // redirect('form-html/category', 'refresh');
                    // echo $this->input->post('cat_id');
                    redirect('admin/pengawas', 'refresh');
                }
                else
                {
                    redirect('admin/pengawas', 'refresh');
                }
            
        }        

    function delete($id){
        if(!empty($id))
        {
          // hapus data
          $this->Pengawas_model->deletePengawas($id);
          redirect('admin/pengawas', 'refresh');
        }
            else
            {
              redirect('admin/pengawas', 'refresh');
            }
        }


        private function hash_password($password) 
        { 
            return get_hash($password, PASSWORD_BCRYPT); 
        } 
}
