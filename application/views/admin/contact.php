  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Contact</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Contact</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
 
    </section>
    <section class="content">
      
      <div class="card">
        <!-- /.card-header -->
        <div class="card-body">
          <div class="row">
            <div class="col-md-3">
              <button type="button" class="btn btn-block btn-outline-primary btn-sm" data-toggle="modal" data-target="#modal-add">Add Contact</button>  
            </div>
            <!-- /.card -->
          </div>
          <table id="contactslist" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>No</th>
              <th>Name</th>
              <th>No Handphone</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <tr>
              <td>1</td>
              <td>Rizki Satriyo
              </td>
              <td>089646544043</td>
              <td> 
                <button type="button" class="btn btn-block btn-outline-primary btn-sm" data-toggle="modal" data-target="#modal-edit">Edit</button>         
              </td>
            </tr>
            </tbody>
            <tfoot>
            <tr>
              <th>No</th>
              <th>Name</th>
              <th>No Handphone</th>
              <th>Action</th>
            </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
    </section>
    </div>

<!-- Memanggil modal edit kontak -->
<?php require_once ('contact_add.php'); ?>
<?php require_once ('contact_edit.php'); ?>