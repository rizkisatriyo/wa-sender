  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Inbox</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">inbox</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
 
    </section>
    <section class="content">
      
      <div class="card">
        <!-- /.card-header -->
        <div class="card-body">
          <div class="row">
            <div class="col-md-3">
              <button type="button" class="btn btn-block btn-outline-primary btn-sm" data-toggle="modal" data-target="#modal-add-message">New Message</button>  
            </div>
            <!-- /.card -->
          </div>
          <table id="contactslist" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>Sender</th>
              <th>Message</th>
              <th></th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <tr>
              <td>Rizki Satriyo</td>
              <td>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</td>
              <td>5 mins ago</td>
              <td> 
                <button type="button" class="btn btn-block btn-outline-danger btn-sm" data-toggle="modal" data-target="#modal-edit">Del</button>         
              </td>

            </tr>
            </tbody>
            <tfoot>
            <tr>
              <th>Sender</th>
              <th>Message</th>
              <th></th>
              <th>Action</th>
            </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
    </section>
    </div>

<!-- Memanggil modal edit kontak -->
<?php require_once ('message_send.php'); ?>
