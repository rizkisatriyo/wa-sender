			<link rel="stylesheet" href="<?php echo base_url('assets');?>/theme/plugins/bootstrap/css/bootstrap.min.css">
			<link rel="stylesheet" href="<?php echo base_url('assets');?>/theme/plugins/font-awesome/css/font-awesome.min.css">
			<link rel="stylesheet" href="<?php echo base_url('assets');?>/theme/fonts/style.css">
			<link rel="stylesheet" href="<?php echo base_url('assets');?>/theme/css/main.css">
			<link rel="stylesheet" href="<?php echo base_url('assets');?>/theme/css/main-responsive.css">
			<link rel="stylesheet" href="<?php echo base_url('assets');?>/theme/plugins/iCheck/skins/all.css">
			<link rel="stylesheet" href="<?php echo base_url('assets');?>/theme/plugins/bootstrap-colorpalette/css/bootstrap-colorpalette.css">
			<link rel="stylesheet" href="<?php echo base_url('assets');?>/theme/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
			<link rel="stylesheet" href="<?php echo base_url('assets');?>/theme/css/theme_light.css" type="text/css" id="skin_color">
			<link rel="stylesheet" href="<?php echo base_url('assets');?>/theme/css/print.css" type="text/css" media="print"/>
			<link rel="stylesheet" href="<?php echo base_url('assets');?>/theme/plugins/DataTables/media/css/DT_bootstrap.css" />
			<link rel="stylesheet" href="<?php echo base_url('assets');?>/theme/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
