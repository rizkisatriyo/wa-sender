<?php

function print_recursive_list($data)
{
    $str = "";
    foreach($data as $list)
    {
        $str .= "<li><a href='javascript:void(0)'>".$list['nama']."</a>";
        $subchild = print_recursive_list($list['child']);
        if($subchild != '')
            $str .= "<ul>".$subchild."</ul>";
        $str .= "</li>";
    }
    return $str;
}

?>