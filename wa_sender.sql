-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 27 Jun 2022 pada 18.08
-- Versi server: 10.4.21-MariaDB
-- Versi PHP: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wa_sender`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_autorespon`
--

CREATE TABLE `tbl_autorespon` (
  `keyword` varchar(100) NOT NULL,
  `answer` text DEFAULT NULL,
  `logic` varchar(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_contact`
--

CREATE TABLE `tbl_contact` (
  `id_contact` int(11) NOT NULL,
  `contact_name` varchar(100) NOT NULL,
  `contact_number` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_contact`
--

INSERT INTO `tbl_contact` (`id_contact`, `contact_name`, `contact_number`) VALUES
(1, 'Kalkulus I', ''),
(2, 'Kalkulus II', ''),
(3, 'Fisika Dasar', ''),
(4, 'Algoritma Pemrograman', ''),
(5, 'Metode Numerik', ''),
(6, 'Uji Kualitas', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_inbox`
--

CREATE TABLE `tbl_inbox` (
  `id` bigint(20) NOT NULL,
  `wa_no` varchar(200) NOT NULL,
  `sub_no` varchar(200) DEFAULT NULL,
  `wa_text` varchar(2000) NOT NULL,
  `wa_time` datetime NOT NULL,
  `status` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_konfigurasi`
--

CREATE TABLE `tbl_konfigurasi` (
  `id_konfigurasi` int(11) NOT NULL,
  `nama_website` varchar(225) NOT NULL,
  `logo` varchar(225) NOT NULL,
  `favicon` varchar(225) NOT NULL,
  `email` varchar(225) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `facebook` varchar(225) NOT NULL,
  `instagram` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_konfigurasi`
--

INSERT INTO `tbl_konfigurasi` (`id_konfigurasi`, `nama_website`, `logo`, `favicon`, `email`, `no_telp`, `alamat`, `facebook`, `instagram`) VALUES
(1, 'WABLAST SKNET', 'member.png', 'admin.png', 'rizki.sa7ria@gmail.com', '089646544043', 'majalengka', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_multi`
--

CREATE TABLE `tbl_multi` (
  `id` bigint(20) NOT NULL,
  `tipe` varchar(1) DEFAULT NULL,
  `profil` varchar(20) DEFAULT NULL,
  `wa_mode` int(11) DEFAULT NULL,
  `wa_no` varchar(200) NOT NULL,
  `sub_no` varchar(200) DEFAULT NULL,
  `wa_text` varchar(2000) NOT NULL,
  `wa_media` varchar(500) DEFAULT NULL,
  `wa_file` varchar(500) DEFAULT NULL,
  `wa_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `id_outbox` bigint(20) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_outbox`
--

CREATE TABLE `tbl_outbox` (
  `id` bigint(20) NOT NULL,
  `wa_mode` int(11) DEFAULT NULL,
  `wa_no` varchar(200) NOT NULL,
  `wa_text` varchar(2000) NOT NULL,
  `wa_media` varchar(500) DEFAULT NULL,
  `wa_file` varchar(500) DEFAULT NULL,
  `wa_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_role`
--

CREATE TABLE `tbl_role` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_role`
--

INSERT INTO `tbl_role` (`id`, `name`, `description`) VALUES
(1, 'panitia', 'Panitia Ujian'),
(2, 'dosen', 'dosen sebagai pengawas');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_sent`
--

CREATE TABLE `tbl_sent` (
  `id` bigint(20) NOT NULL,
  `wa_mode` int(11) DEFAULT NULL,
  `wa_no` varchar(200) NOT NULL,
  `wa_text` varchar(2000) NOT NULL,
  `wa_media` varchar(500) DEFAULT NULL,
  `wa_file` varchar(500) DEFAULT NULL,
  `wa_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `status` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_tmp`
--

CREATE TABLE `tbl_tmp` (
  `tmp_cd` varchar(50) NOT NULL,
  `tmp_val` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `id_role` int(11) NOT NULL DEFAULT 2,
  `nidn` varchar(25) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL DEFAULT '$2y$05$WGJb.tDgy7jIhte7sxWGFe4nV/wiBVr7DbSApg2M.W5v1IcoVSWVa',
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(254) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `activation_code` varchar(50) NOT NULL,
  `forgotten_password_code` varchar(50) NOT NULL,
  `forgotten_password_time` datetime NOT NULL,
  `remember_code` varchar(50) NOT NULL,
  `created_on` datetime NOT NULL,
  `last_login` datetime NOT NULL,
  `active` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `id_role`, `nidn`, `username`, `password`, `first_name`, `last_name`, `email`, `phone`, `photo`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`) VALUES
(1, 1, '', 'Administrator', '$2y$05$WGJb.tDgy7jIhte7sxWGFe4nV/wiBVr7DbSApg2M.W5v1IcoVSWVa', 'rizki', 'satriyo', 'admin@admin.com', '089646544043', '1526456245974.png', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '2018-05-22 20:58:33', 1);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tbl_autorespon`
--
ALTER TABLE `tbl_autorespon`
  ADD PRIMARY KEY (`keyword`);

--
-- Indeks untuk tabel `tbl_contact`
--
ALTER TABLE `tbl_contact`
  ADD PRIMARY KEY (`id_contact`);

--
-- Indeks untuk tabel `tbl_inbox`
--
ALTER TABLE `tbl_inbox`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indeks untuk tabel `tbl_konfigurasi`
--
ALTER TABLE `tbl_konfigurasi`
  ADD PRIMARY KEY (`id_konfigurasi`);

--
-- Indeks untuk tabel `tbl_multi`
--
ALTER TABLE `tbl_multi`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_outbox`
--
ALTER TABLE `tbl_outbox`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_role`
--
ALTER TABLE `tbl_role`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tbl_sent`
--
ALTER TABLE `tbl_sent`
  ADD PRIMARY KEY (`id`,`wa_time`);

--
-- Indeks untuk tabel `tbl_tmp`
--
ALTER TABLE `tbl_tmp`
  ADD PRIMARY KEY (`tmp_cd`);

--
-- Indeks untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tbl_contact`
--
ALTER TABLE `tbl_contact`
  MODIFY `id_contact` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `tbl_inbox`
--
ALTER TABLE `tbl_inbox`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbl_konfigurasi`
--
ALTER TABLE `tbl_konfigurasi`
  MODIFY `id_konfigurasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tbl_multi`
--
ALTER TABLE `tbl_multi`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbl_outbox`
--
ALTER TABLE `tbl_outbox`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `tbl_role`
--
ALTER TABLE `tbl_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
